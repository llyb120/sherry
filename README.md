## Sherry

Sherry 是基于GO编写一种简单而灵活的脚本语言，具有动态类型和直观的语法。

### 特性

1. 动态类型：Sherry 是一种动态类型语言，变量无需显式声明类型。
2. 直观语法：Sherry 的语法简洁明了，基本和GO一样，无需额外的学习成本。
3. 内置数据结构：Sherry 提供了Javascript中的数组和对象方便的数据结构。
4. 函数式编程：Sherry 支持函数式编程，可以方便地进行函数嵌套和闭包操作。
6. 便于集成，单文件无任何依赖，可以轻松引入项目而不担心冲突。

<img src="./sherry.png" alt="Sherry" width="300"/>


### 快速上手

```
package main

func main() {
    evaluator := NewEvaluator()
    evaluator.EvalString("println('hello world')")
}
```

### 基础语法

#### 1. 变量声明

```
a = 1
b = 2
c = a + b
d = "hello"
e = [1, 2, 3]
f = {
    name: "张三",
    age: 18
}
```

#### 2. 条件语句

```
if a > b {
    println('a > b')
} else if a == b {
    println('a == b')
} else {
    println('a < b')
}

// 复杂条件
if a > b && b > c {
    println('a > b && b > c')
} else if a == b || b == c {
    println('a == b || b == c')
} else {
    println('a < b')
}
```


#### 3. 循环语句

```
for i = 0; i < 10; i++ {
    println(i)
    if i == 5 {
        break
    }
}

i = 0
for i < 10 {
    println(i)
    i = i + 1
    if i == 5 {
        continue
    }
}

```

#### 4. 函数定义

```
// 直接定义函数
func add(a, b) {
    return a + b
}

// 使用变量定义
minus = func(a, b) {
    return a - b
}
```

#### 5. 调用函数

```
add(1, 2)
minus(1, 2)

// 不同于GO的是，Sherry 目前最多支持一个返回值，如果需要返回多个值，请使用数组或对象
```

#### 6. 数组

```
// 数组中可以包含任意类型，包括函数
arr = [1, 2, {
    name: "张三",
    age: 18,
    fn: func(a, b) {
        return a + b
    }
}]
println(arr[0])
```

#### 7. 对象

```
// 对象中可以包含任意类型，包括函数
obj = {
    name: "张三",
    age: 18,
    arr: [1, 2, 3],
    fn: func(a, b) {
        return a + b
    }
}
println(obj.name)
println(obj.arr[0])
println(obj.func(1, 2))
```

#### 8. 闭包

```
fn = func(a, b) {
    return func(c) {
        return a + b + c
    }
}

closure = fn(1, 2)
r = closure(3)
println(r)

each = func(arr, handler) {
    result = []
    for i = 0; i < len(arr); i++ {
        result[] = handler(arr[i])
    }
    return result
}
ret = each([1, 2, 3], func(item) {
    return item * 2
})
println(ret)
```

### 内置函数

#### 1. println

```
println('hello world')
```

#### 2. print

```
print('hello world')
```

#### 3. len

```
len([1, 2, 3])
```

### 自定义函数绑定

```
package main

func main() {
    evaluator := NewEvaluator()
    // 绑定变量
    evaluator.Env.Set("a", 1)
    evaluator.Env.Set("b", map[string]interface{}{
        "name": "张三",
        "age": 18,
    })
    // 绑定函数
    evaluator.Env.Set("test", func(args ...interface{}) []interface{} {
        for _, arg := range args {
            fmt.Println(arg)
        }
        return nil
    })
    evaluator.EvalString("test(1, 2, 3)")

    // 另一种绑定方式
    evaluator.EvalString(`test = func(a, b) { return a + b };`)
    ret, err := evaluator.EvalMethod("test", 1.0, 2.0)
    if err != nil {
        t.Errorf("Test failed: %s\n", err)
    }
}
```


